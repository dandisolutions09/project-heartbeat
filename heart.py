
import requests
import time

def make_get_request(url):
    try:
        start_time = time.time()  # Record the start time

        # Sending GET request
        response = requests.get(url)

        # Calculate the time taken
        end_time = time.time()
        elapsed_time = end_time - start_time

        # Checking if the request was successful (status code 200)
        if response.status_code == 200:
            print(f"GET request to {url} successful! Elapsed time: {elapsed_time:.2f} seconds")
            print("Response:")
            print(response)
        else:
            print(f"GET request to {url} failed with status code {response.status_code}. Elapsed time: {elapsed_time:.2f} seconds")

    except requests.RequestException as e:
        print(f"An error occurred: {e}")



   
        
        
if __name__ == "__main__":
    # URL for the GET request
#    url_1 = 'https://nurse-backend.onrender.com/get-nurses'
 #   url_2 = 'https://new-backend-c3hj.onrender.com/get-data'
    url_3 = 'https://rems-backend.onrender.com/get-userbyid/65d2198ff133d74f569934fd'

    # Infinite loop with a 10-second delay
    while True:
        
        start_time_1 = time.time()  # Record the start time
#        make_get_request(url_1)
 #       make_get_request(url_2)
        make_get_request(url_3)
        
        end_time_1 = time.time()
        elapsed_time_1 = end_time_1 - start_time_1
        
       
        time.sleep(10)
        print("checking time:", elapsed_time_1)
